<?php


class CalendarController extends ControllerBase
{
    public function indexAction()
    {
        $calendar_day = Calendar::findFirst();
        $current_date = new DateTime();
        $current_year = $current_date->format('Y');
    
        $months = [
            '01' => 'Jan', '02' => 'Feb',
            '03' => 'Mar', '04' => 'Apr',
            '05' => 'May', '06' => 'Jun',
            '07' => 'Jul', '08' => 'Aug',
            '09' => 'Sep', '10' => 'Oct',
            '11' => 'Nov', '12' => 'Dec',
        ];
        $days_of_week = [
            'Mon',
            'Tue',
            'Wed',
            'Thu',
            'Fri',
            'Sat',
            'Sun'
        ];
        
        $all_months = [];
        $all_days = [];
        
        foreach ($months as $num_month => $month_full_name) {
            $date = new DateTime($current_year . '-' . $num_month);
            $month = $date->format('m');
            $day = $date->format('d');
            $days_in_month = date("t",mktime(0,0,0,$month,$day,$current_year));
            $all_months[] = $num_month;
            for ($i = 1; $i <= $days_in_month; $i++) {
                $new_date = new DateTime();
                $new_date->setDate($current_year, $month, $i);
                $all_days[] = $new_date;
            }
        }
        
        $this->view->days_of_week = $days_of_week;
        $this->view->year = $current_year;
        $this->view->months = $months;
        $this->view->all_days = $all_days;
        if ($calendar_day != 0) {
            $this->view->calendar_day = $calendar_day;
        }
    }
    
    public function storeAction()
    {
        if ($this->request->isPost()) {
           $data = Calendar::storeDestroy($this->request->get('date'));
        }
        return $this->response->setJsonContent($data);
    }
}