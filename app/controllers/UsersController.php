<?php


class UsersController extends ControllerBase
{
    public function createAction()
    {
        $form = new UserCreateForm();
        
        if ($this->request->getPost()) {
            if ($form->isValid($this->request->getPost())) {
                $user = new Users([
                    'name' => $this->request->getPost('name', 'striptags'),
                    'login' => $this->request->getPost('login'),
                    'email' => $this->request->getPost('email'),
                    'password' => $this->security->hash($this->request->getPost('password'))
                ]);
                
                if ($user->save()) {
                    $this->flash->success("User was created successfully");
                    return $this->dispatcher->forward([
                        'controller' => 'staff',
                        'action' => 'index'
                    ]);
                }
                
                foreach ($user->getMessages() as $message) {
                    $this->flash->error($message);
                }
            } else {
                foreach ($form->getMessages() as $message) {
                    $this->flash->error($message);
                }
            }
        }
        
        $this->view->setVar('form', $form);
    }
    
    public function editAction($id)
    {
        $user = Users::findFirst([
            'id = :id:',
            'bind' => [
                'id' => $id
            ]
        ]);
        
        if (!$user) {
            $this->flash->error('User not found');
            
            return $this->dispatcher->forward([
                'controller' => 'staff',
                'action' => 'index'
            ]);
        }
        
        $form = new UserUpdateForm($user,[
            'edit' => true
        ]);
        
        if ($this->request->isPost()) {
            $user->assign([
                'name' => $this->request->getPost('name', 'striptags'),
                'login' => $this->request->getPost('login'),
                'email' => $this->request->getPost('email'),
                'is_deleted' => $this->request->getPost('is_deleted'),
            ]);
            
            if (!$form->isValid($this->request->getPost())) {
                foreach ($form->getMessages() as $message) {
                    $this->flash->error($message);
                }
            } else {
                if (!$user->save()) {
                    foreach ($user->getMessages() as $message) {
                        $this->flash->error($message);
                    }
                } else {
                    $this->flash->success('User was updated successfully');
                }
            }
        }
        
        $this->view->setVars([
            'user' => $user,
            'form' => $form
        ]);
    }
    
    public function changePasswordAction()
    {
        $user_id = $this->session->get('auth')['id'];
        $user = Users::findFirst([
            'id = :id:',
            'bind' => [
                'id' => $user_id
            ]
        ]);
        
        $form = new ChangePasswordForm();
        
        if ($this->request->isPost()) {
            $user->assign([
                'password' => $this->security->hash($this->request->getPost('password')),
            ]);

            if (!$form->isValid($this->request->getPost())) {
                foreach ($form->getMessages() as $message) {
                    $this->flash->error($message);
                }
            } else {
                if (!$user->save()) {
                    foreach ($user->getMessages() as $message) {
                        $this->flash->error($message);
                    }
                } else {
                    $this->flash->success('Password was updated successfully');
                }
            }
        }
        
        $this->view->setVar('form', $form);
    }
}