<?php


class StaffController extends ControllerBase
{
    public function indexAction()
    {
        $all_users = Users::find();
        $auth_user = $this->session->get('auth');
        
        $user_in_session = Users::findFirst([
            'conditions' => 'id = :id:',
            'bind' => [
                'id' => $auth_user['id']
            ]
        ]);
        
        $users[] = $user_in_session;
        
        foreach ($all_users as $user) {
            if ($user->id != $user_in_session->id) {
                $users[] = $user;
            }
        }
    
        if ($this->request->isPost()) {
            $current_time = new DateTime($this->request->get('year') . '-' . $this->request->get('month'));
        } else {
            $current_time = new DateTime();
        }
        
        $current_year = $current_time->format('Y');
        $current_month = $current_time->format('m');
        $current_day = $current_time->format('d');
        $days_in_month = date("t",mktime(0,0,0,$current_month,$current_day,$current_year));
        
        $data_days = Times::countDaysOfMonthAndWorkingDays($days_in_month, $current_year, $current_month);
        $days_of_month = $data_days['days_of_month'];
        $working_days = $data_days['working_days'];
    
        $working_hours_per_day = 8;
        $working_hours_per_month = $working_days * $working_hours_per_day;
        
        $months = [
            '01' => 'January',   '02' => 'February',
            '03' => 'March',     '04' => 'April',
            '05' => 'May',       '06' => 'June',
            '07' => 'July',      '08' => 'August',
            '09' => 'September', '10' => 'October',
            '11' => 'November',  '12' => 'December',
        ];
        
        $hours_log = Times::countHoursLog(
            $auth_user['id'],
            $current_year,
            $current_month,
            $working_hours_per_month);

        $current_date_time = new DateTime();
        $years = Times::getLastTenYears();
        
        $this->view->percentage_of_working_hours = $hours_log['percentage_of_working_hours'];
        $this->view->lateness = $hours_log['lateness'];
        $this->view->total_hours = $hours_log['total_hours'];
        $this->view->working_hours_per_month = $working_hours_per_month;
        $this->view->days_of_month = $days_of_month;
        $this->view->users = $users;
        $this->view->months = $months;
        $this->view->current_time = getdate();
        $this->view->current_date_time = $current_date_time->setTimezone(new DateTimeZone('Asia/Bishkek'));
        $this->view->years = $years;
    
        if ($this->request->isPost()) {
            $this->view->disable();
            return  $this->view->partial('staff/partials/staff_table', [
                'days_of_month' => $days_of_month,
                'lateness' => $hours_log['lateness'],
                'total_hours' => $hours_log['total_hours'],
                'percentage_of_working_hours' => $hours_log['percentage_of_working_hours']
            ]);
        }
    }
}