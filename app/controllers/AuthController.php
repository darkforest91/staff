<?php


class AuthController extends ControllerBase
{
    public function loginAction()
    {
        $form = new LoginForm();
        
        if ($this->request->isPost()) {
            try {
                if ($form->isValid($this->request->getPost()) == false) {
                    foreach ($form->getMessages() as $message) {
                        $this->flash->error((string) $message);
                    }
                } else {
                    $email = $this->request->getPost('email');
                    $password = $this->request->getPost('password');
                    
                    $user = Users::findFirst([
                        "email = :email:",
                        'bind' => [
                            'email' => $email,
                        ]
                    ]);
                    
                    if ($user && $this->security->checkHash($password, $user->password)) {
                        $this->session->set('auth', [
                            'id' => $user->id,
                            'name' => $user->name,
                            'is_admin' => $user->is_admin,
                            'is_deleted' => $user->is_deleted
                        ]);
                        return $this->response->redirect('staff/index');

                    }
                    $this->flash->error('Wrong email/password');
                    
                }
            } catch (\Exception $e) {
                $this->flash->error($e->getMessage());
            }
        }
        
        $this->view->setVar('form', $form);
    }
    
    public function logoutAction()
    {
        $this->session->remove('auth');
        $this->flash->success('Goodbye');
        
        $this->dispatcher->forward([
            'controller' => 'index',
            'action' => 'index'
        ]);
    }
}