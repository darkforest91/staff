<?php


class TimeController extends ControllerBase
{
    public function createAction()
    {
        if ($this->request->isPost()) {
         $data_time = Times::createTime(
             $this->request->get('date'),
             $this->request->get('time'),
             $this->request->get('user_id'),
             $this->request->get('event'),
             $this->request->get('time_id'));
        }
        
        return $this->response->setJsonContent($data_time);
    }
    
    public function editAction()
    {
        if ($this->request->isPost()) {
            $data_time = Times::editTime(
                $this->request->get('time_id'),
                $this->request->get('time'),
                $this->request->get('event'));
        }
        
        return $this->response->setJsonContent($data_time);
    }
    
}