{% if session.has('auth') %}
    {% set i = 0 %}

    {% if (session.get('auth')['is_admin']) %}
        <p class="text-right mr-5">{{ link_to('users/create', 'Create new user') }}</p>
    {% endif %}

    <div class="ml-5 mr-5" style="max-width: 380px">
        <h2 class="mb-5">My hours log</h2>

        <div class="mb-4 my-hours-log">
            <p class='user_hours_log'>You have: {{ total_hours }}</p>
            <p class='user_hours_log'>You have/Assigned: {{ percentage_of_working_hours }}%</p>
            <p class='user_hours_log'>Assigned: {{ working_hours_per_month }}</p>
            <p class='user_hours_log'>You are late: {{ lateness }}</p>
        </div>

        <p class='user_hours_log font-weight-bold'>You must be at work before 9:00. If you are late more than 3 times a month,
            then the discipline will be considered unsatisfactory and negative
            will affect the request for a salary increase.</p>
        <p>{{ link_to('calendar/index', 'Calendar') }}</p>
    </div>

    <div class="row flex justify-content-center mb-5">
        <select class="mr-5 date-filter" name="month" id="month-filter">
            {% for month in months %}
                {% set i = i + 1 %}
                <option value="{{ i }}" {% if (current_time['month'] == month) %} selected {% endif %}>{{ month }}</option>
            {% endfor %}
        </select>

        <select class="ml-5 date-filter" name="year" id="year-filter">
            {% for year in years %}
                <option value="{{ year }}" {% if (current_time['year'] == year) %} selected {% endif %}>{{ year }}</option>
            {% endfor %}
        </select>
    </div>

    <div id="staff_table">
    {{ partial('staff/partials/staff_table') }}
    </div>

{% endif %}