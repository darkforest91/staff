{{ flash.output() }}
<div class="page-header mb-5 mt-5">
    <h1 class="text-center">Welcome to staff!</h1>
</div>

{% if (session.has('auth') == false) %}
    <p class="text-center">Enter the application: <span class="h4">{{ link_to('auth/login', 'Login') }}</span></p>
{% else %}
    <p class="text-center">Enter the application: <span class="h4">{{ link_to('staff/index', 'Enter') }}</span></p>
{% endif %}


