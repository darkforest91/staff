{% set i = 0 %}
<div>
    <p class="text-center">Calendar {{ year }}</p>
</div>
<div class="container">
    <div class="row">
        {% for month in months %}
            {% set i = i + 1 %}
        <div class="col-3 mb-5">
            <h5 class="text-center">{{ month }}</h5>
            <table class="table-sm text-center">
                <thead>
                <tr>
                    {% for day_of_week in days_of_week %}
                    <th scope="col">{{ day_of_week }}</th>
                    {% endfor %}
                </tr>
                </thead>
                <tbody>
                <tr>
                    {% for day_of_week in days_of_week %}
                    <td>
                        {% for day in all_days %}
                            {% if (day_of_week == day.format('D')) and month == day.format('M') %}
                            <p class="calendar" id="calendar-{{ day.format('Y-m-d') }}" {% if (session.get('auth')['is_admin']) %} onclick="calendar('calendar', 'calendar-{{ day.format('Y-m-d') }}')" data-date="{{ day.format('Y-m-d') }}" {% endif %}
                                {% if (day_of_week == 'Sat' or day_of_week == 'Sun') %} style="background-color: #FF6666" {% endif %}
                                    {% if (calendar_day != null) %}{% if (calendar_day.checkDate(day.format('Y-m-d')) == day.format('Y-m-d')) %} style="background-color: greenyellow" {% endif %} {% endif %}>
                                {{ day.format('d') }}
                            </p>
                            {% endif %}
                        {% endfor %}
                    </td>
                    {% endfor %}
                </tr>
                </tbody>
            </table>
        </div>
        {% endfor %}
    </div>
</div>

<p class="ml-5">{{ link_to('/staff/index', 'Back') }}</p>