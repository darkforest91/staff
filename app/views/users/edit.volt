<div class="container mt-3">
    <form method="post" class="w-50 mt-5">
        <div class="form-group row">
            <label for="name" class="col-sm-2 col-form-label">Name:</label>
            <div class="col-sm-10">
                {{ form.render('name', ['class': 'form-control', 'placeholder': 'Name']) }}
            </div>
        </div>

        <div class="form-group row">
            <label for="name" class="col-sm-2 col-form-label">Login:</label>
            <div class="col-sm-10">
                {{ form.render('login', ['class': 'form-control', 'placeholder': 'Login']) }}
            </div>
        </div>

        <div class="form-group row">
            <label for="name" class="col-sm-2 col-form-label">Email:</label>
            <div class="col-sm-10">
                {{ form.render('email', ['class': 'form-control', 'placeholder': 'Email']) }}
            </div>
        </div>

        <div class="form-group row">
            <label for="name" class="col-sm-2 col-form-label">Works/Fires:</label>
            <div class="col-sm-10">
                {{ form.render('is_deleted', ['class': 'form-control w-50']) }}
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-10">
                {{ form.render('Edit') }}
                <div class="text-right">
                    <p>{{ link_to('staff/index', 'Back') }}</p>
                </div>
            </div>
        </div>
    </form>
</div>