<div class="container mt-3">
    <h1 class="mt-3">Change Password</h1>

    {{ flash.output() }}

    <form method="post">
        <div class="form-group row">
            {{ form.label('password', ['class': 'col-sm-2 col-form-label']) }}
            <div class="col-sm-10">
                {{ form.render('password', ['class': 'form-control w-50', 'placeholder': 'New password']) }}
            </div>
        </div>

        <div class="form-group row">
            {{ form.label('confirmPassword', ['class': 'col-sm-2 col-form-label']) }}
            <div class="col-sm-10">
                {{ form.render('confirmPassword', ['class': 'form-control w-50', 'placeholder': 'Confirm Password']) }}
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-10">
                {{ form.render("Change") }}
            </div>
        </div>
    </form>
</div>