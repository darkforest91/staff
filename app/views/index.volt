<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Staff</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo $this->url->get('img/favicon.ico')?>"/>

        <style>
            body {
                background-color: #FFFFE0;
            }
            .user_hours_log {
                margin-bottom: 0
            }

            .big-table table {
                white-space: nowrap;
            }
        </style>
    </head>
    <body>

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-3">
        {{ link_to(null, 'class': 'navbar-brand', 'Staff') }}

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item">

                </li>
            </ul>

            <ul class="navbar-nav my-2 my-lg-0">
                {%- if session.has('auth') -%}
                    <li class="nav-item dropdown">
                        <div class="btn-group">
                            <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ session.get('auth')['name'] }}
                            </button>
                            <div class="dropdown-menu dropdown-menu-right">
                                <button class="dropdown-item" type="button">{{ link_to('auth/logout', 'class': 'dropdown-item', 'Logout') }}</button>
                                <button class="dropdown-item" type="button">{{ link_to('users/changePassword', 'class': 'dropdown-item', 'Change password') }}</button>
                            </div>
                        </div>
                    </li>
                {% else %}
                    <li class="nav-item">{{ link_to('auth/login', 'class': 'nav-link', 'Login') }}</li>
                {% endif %}
            </ul>
        </div>
    </nav>

    <div>
        {{ flash.output() }}
        {{ content() }}
    </div>
    <!-- jQuery first, then Popper.js, and then Bootstrap's JavaScript -->
    <script
            src="https://code.jquery.com/jquery-3.5.1.min.js"
            integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script type="text/javascript" src="../js/table_staff.js"></script>
    <script type="text/javascript" src="../js/calendar.js"></script>
    </body>
</html>
