<?php


use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Form;
use Phalcon\Validation\Validator\Confirmation;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\Identical;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;

class UserCreateForm extends Form
{
    public function initialize()
    {
        $name = new Text('name', [
            'placeholder' => 'Name'
        ]);
        $name->addValidators([
            new PresenceOf([
                'message' => 'The name is required'
            ]),
            new StringLength([
                'min' => 3,
                'messageMinimum' => 'Minimum 3 characters'
            ]),
            new StringLength([
                'max' => 128,
                'messageMaximum' => 'Maximum 128 characters'
            ])
        ]);
        $this->add($name);
    
        $login = new Text('login', [
            'placeholder' => 'Login'
        ]);
        $login->addValidators([
            new StringLength([
                'max' => 128,
                'messageMaximum' => 'Maximum 128 characters'
            ])
        ]);
        $this->add($login);
        
        $email = new Text('email');
        $email->addValidators([
            new PresenceOf([
                'message' => 'The email is required'
            ]),
            new Email([
                'message' => 'The email is not valid'
            ])
        ]);
        $this->add($email);
        
        $password = new Password('password');
        $password->addValidators([
            new PresenceOf([
                'message' => 'The password is required'
            ]),
            new StringLength([
                'min' => 8,
                'messageMinimum' => 'Minimum 8 characters'
            ]),
            new Confirmation([
                'message' => 'Password does not confirmation',
                'with' => 'confirmPassword'
            ])
        ]);
        $this->add($password);
        
        $confirmPassword = new Password('confirmPassword');
        $confirmPassword->addValidators([
            new PresenceOf([
                'message' => 'The confirmation password is required'
            ])
        ]);
        $this->add($confirmPassword);
        
        $csrf = new Hidden('csrf');
        $csrf->addValidator(new Identical([
            'value' => $this->security->getRequestToken(),
            'messages' => 'CSRF validation failed'
        ]));
        $csrf->clear();
        $this->add($csrf);
        
        $this->add(new Submit('Create', [
            'class' => 'btn btn-success'
        ]));
    }
    
//    /**
//     * @param string $name
//     * @return \Phalcon\Messages\MessageInterface|string
//     */
//    public function messages(string $name)
//    {
//        if ($this->hasMessagesFor($name)) {
//            foreach ($this->getMessagesFor($name) as $message) {
//                return $message;
//            }
//        }
//
//        return '';
//    }
}