<?php


use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Form;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;

class UserUpdateForm extends Form
{
    public function initialize()
    {
        $name = new Text('name', [
            'placeholder' => 'Name'
        ]);
        $name->addValidators([
            new PresenceOf([
                'message' => 'The name is required'
            ]),
            new StringLength([
                'min' => 3,
                'messageMinimum' => 'Minimum 3 characters'
            ]),
            new StringLength([
                'max' => 128,
                'messageMaximum' => 'Maximum 128 characters'
            ])
        ]);
        $this->add($name);
    
        $login = new Text('login', [
            'placeholder' => 'Login'
        ]);
        $login->addValidators([
            new StringLength([
                'max' => 128,
                'messageMaximum' => 'Maximum 128 characters'
            ])
        ]);
        $this->add($login);
    
        $email = new Text('email');
        $email->addValidators([
            new PresenceOf([
                'message' => 'The email is required'
            ]),
            new Email([
                'message' => 'The email is not valid'
            ])
        ]);
        $this->add($email);
    
        $this->add(new Submit('Edit', [
            'class' => 'btn btn-success'
        ]));
        
        $is_deleted = new Select('is_deleted', [
            false => 'Works',
            true => 'Fires'
        ]);
        $this->add($is_deleted);
    }
}