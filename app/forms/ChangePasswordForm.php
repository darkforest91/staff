<?php


use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Form;
use Phalcon\Validation\Validator\Confirmation;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;

class ChangePasswordForm extends Form
{
    public function initialize()
    {
        $password = new Password('password');
        $password->addValidators([
            new PresenceOf([
                'message' => 'The password is required'
            ]),
            new StringLength([
                'min' => 8,
                'messageMinimum' => 'Minimum 8 characters'
            ]),
            new Confirmation([
                'message' => 'Password does not confirmation',
                'with' => 'confirmPassword'
            ])
        ]);
        $this->add($password);
    
        $confirmPassword = new Password('confirmPassword');
        $confirmPassword->addValidators([
            new PresenceOf([
                'message' => 'The confirmation password is required'
            ])
        ]);
        $this->add($confirmPassword);
    
        $this->add(new Submit('Change', [
            'class' => 'btn btn-success'
        ]));
    }
}