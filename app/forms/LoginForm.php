<?php


use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Form;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\Identical;
use Phalcon\Validation\Validator\PresenceOf;

class LoginForm extends Form
{
    public function initialize()
    {
        $email = new Text('email', [
            'placeholder' => 'Email'
        ]);
        $email->addValidators([
            new PresenceOf([
                'message' => 'Email is required'
            ]),
            new Email([
                'messages' => 'Email is nor valid'
            ])
        ]);
        $this->add($email);
        
        $password = new Password('password', [
            'placeholder' => 'Password'
        ]);
        $password->addValidator(
            new PresenceOf([
                'message' => 'The password is required'
            ])
        );
        $password->clear();
        $this->add($password);
        
        $csrf = new Hidden('csrf');
        $csrf->addValidator(new Identical([
            'value' => $this->security->getRequestToken(),
            'message' => 'CSRF validation fails'
        ]));
        $csrf->clear();
        $this->add($csrf);
        $this->add(new Submit('Login', [
            'class' => 'btn btn-primary'
        ]));
    }
}