<?php

$router = $di->getRouter();

$router->add('/time/create', [
    'controller' => 'time',
    'action' => 'create'
]);

$router->add('/time/edit', [
    'controller' => 'time',
    'action' => 'edit'
]);

$router->add('calendar/store', [
    'controller' => 'calendar',
    'action' => 'store'
]);

$router->handle($_SERVER['REQUEST_URI']);
