<?php

use Phalcon\Acl\Adapter\Memory as AclList;
use Phalcon\Acl\Component;
use Phalcon\Acl\Enum;
use Phalcon\Acl\Role;
use Phalcon\Di\Injectable;
use Phalcon\Events\Event;
use Phalcon\Mvc\Dispatcher;

class SecurityPlugin extends Injectable
{
    /**
     * @param Event $event
     * @param Dispatcher $dispatcher
     * @return bool
     */
    public function beforeExecuteRoute(Event $event, Dispatcher $dispatcher)
    {
        $auth = $this->session->get('auth');
    
        if (!$auth || $auth['is_deleted']) {
            $role = 'Guests';
        } elseif ($auth['is_admin']) {
            $role = 'Admins';
        } else {
            $role = 'Users';
        }
    
        $controller = $dispatcher->getControllerName();
        $action = $dispatcher->getActionName();
    
        $acl = $this->getAcl();
    
        if (!$acl->isComponent($controller)) {
            $dispatcher->forward([
                'controller' => 'index',
                'action' => 'index',
            ]);
    
            return false;
        }
    
        $allowed = $acl->isAllowed($role, $controller, $action);
        if (!$allowed) {
            $dispatcher->forward([
                'controller' => 'index',
                'action'     => 'index',
            ]);
        
            $this->session->destroy();
        
            return false;
        }
    
        return true;
    }
    
    protected function getAcl()
    {
        if (isset($this->persistent->acl)) {
            return $this->persistent->acl;
        }
    
        $acl = new AclList();
        $acl->setDefaultAction(Enum::DENY);
    
        $roles = [
            'users'  => new Role(
                'Users'
            ),
            'guests' => new Role(
                'Guests'
            ),
            'admins' => new Role(
                'Admins'
            )
        ];
    
        foreach ($roles as $role) {
            $acl->addRole($role);
        }
    
        $users_resources = [
            'staff'    => ['index'],
            'time'     => ['create'],
            'auth'     => ['logout'],
            'users'    => ['changePassword'],
            'calendar' => ['index']
        ];
        foreach ($users_resources as $resource => $actions) {
            $acl->addComponent(new Component($resource), $actions);
        }
    
        $admins_resources = [
            'staff'    => ['index'],
            'time'     => ['create', 'edit'],
            'auth'     => ['logout'],
            'users'    => ['changePassword', 'create', 'edit'],
            'calendar' => ['index', 'store']
        ];
    
        foreach ($admins_resources as $resource => $actions) {
            $acl->addComponent(new Component($resource), $actions);
        }
        
        $public_resources = [
            'index'      => ['index'],
            'auth'      => ['login'],
        ];
        
        foreach ($public_resources as $resource => $actions) {
            $acl->addComponent(new Component($resource), $actions);
        }
    
        foreach ($roles as $role) {
            foreach ($public_resources as $resource => $actions) {
                foreach ($actions as $action) {
                    $acl->allow($role->getName(), $resource, $action);
                }
            }
        }
    
        foreach ($users_resources as $resource => $actions) {
            foreach ($actions as $action) {
                $acl->allow('Users', $resource, $action);
            }
        }
    
        foreach ($admins_resources as $resource => $actions) {
            foreach ($actions as $action) {
                $acl->allow('Admins', $resource, $action);
            }
        }
    
        $this->persistent->acl = $acl;
    
        return $acl;
    }
}