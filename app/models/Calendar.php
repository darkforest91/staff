<?php


use Phalcon\Mvc\Model;

class Calendar extends Model
{
    public $id;
    public $date;
    
    /**
     * @inheritDoc
     */
    public function getConnectionService()
    {
        // TODO: Implement getConnectionService() method.
    }
    
    /**
     * @inheritDoc
     */
    public function getConnection()
    {
        // TODO: Implement getConnection() method.
    }
    
    /**
     * @inheritDoc
     */
    public function dumpResult($base, $result)
    {
        // TODO: Implement dumpResult() method.
    }
    
    /**
     * @inheritDoc
     */
    public function setForceExists($forceExists)
    {
        // TODO: Implement setForceExists() method.
    }
    
    /**
     * @param $date
     */
    static function storeDestroy($date)
    {
        $calendar_day = Calendar::findFirst([
            'conditions' => 'date = :date:',
            'bind' => [
                'date' => $date
            ]
        ]);
        
        if ($calendar_day->date != $date) {
            $calendar = new Calendar([
                'date' => $date
            ]);
            $calendar->save();
            
            return $calendar->date;
        } else {
            $calendar_day->delete();
            return 0;
        }
    }
    
    static function checkDate($date)
    {
        $calendar_day = Calendar::findFirst([
            'conditions' => 'date = :date:',
            'bind' => [
                'date' => $date
            ]
        ]);
        
        return $calendar_day->date;
    }
}