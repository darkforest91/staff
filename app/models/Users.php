<?php


use Phalcon\Mvc\Model;
use Phalcon\Validation;
use Phalcon\Validation\Validator\Uniqueness;

class Users extends Model
{
    public $id;
    public $name;
    public $login;
    public $email;
    public $password;
    public $is_deleted;
    
    public function validation()
    {
        $validator = new Validation();
        
        $validator->add('email', new Uniqueness([
            "message" => "The email is already registered",
        ]));
        
        return $this->validate($validator);
    }
    
    public function initialize()
    {
        $this->hasMany('id', Times::class, 'user_id', [
            'alias' => 'times'
        ]);
    }
    
    /**
     * @inheritDoc
     */
    public function getConnectionService()
    {
        // TODO: Implement getConnectionService() method.
    }
    
    /**
     * @inheritDoc
     */
    public function getConnection()
    {
        // TODO: Implement getConnection() method.
    }
    
    /**
     * @inheritDoc
     */
    public function dumpResult($base, $result)
    {
        // TODO: Implement dumpResult() method.
    }
    
    /**
     * @inheritDoc
     */
    public function setForceExists($forceExists)
    {
        // TODO: Implement setForceExists() method.
    }
}