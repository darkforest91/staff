<?php


use Phalcon\Mvc\Model;

class Times extends Model
{
    public $id;
    public $start_time;
    public $end_time;
    public $date;
    public $user_id;
    public $total_time;
    public $is_late;
    
    public function initialize()
    {
        $this->belongsTo('user_id', Users::class, 'id', [
            'alias' => 'user'
        ]);
    }
    
    /**
     * @inheritDoc
     */
    public function getConnectionService()
    {
        // TODO: Implement getConnectionService() method.
    }
    
    /**
     * @inheritDoc
     */
    public function getConnection()
    {
        // TODO: Implement getConnection() method.
    }
    
    /**
     * @inheritDoc
     */
    public function dumpResult($base, $result)
    {
        // TODO: Implement dumpResult() method.
    }
    
    /**
     * @inheritDoc
     */
    public function setForceExists($forceExists)
    {
        // TODO: Implement setForceExists() method.
    }
    
    /**
     * @param $date
     * @param $time
     * @param $user_id
     * @throws Exception
     */
    static function createTime($date, $time, $user_id, $event, $time_id)
    {
        if($event==="start-button"){
            $time_model = new Times([
                'user_id' => $user_id,
                'date' => $date,
                'start_time' => $time
            ]);
            $time_model->is_late = self::checksForLateness($time, $date, $user_id);
            $time_model->save();
            
        } else{
            $time_model = Times::findFirst([
                'conditions' => 'id = :id:',
                'bind' => [
                    'id' => $time_id,
                ]
            ]);
            $time_model->end_time = $time;
            $time_model->total_time = self::countTotalMinutesForTimeTable($time_model->start_time, $time_model->end_time, $time_model->date);
            $time_model->save();
        }
        
        $total_time = self::countTotalTime($time_model->user_id, $time_model->date);
        
        $data_time = [
            'time_id' => $time_model->id,
            'user_id' => $time_model->user_id,
            'date' => $time_model->date,
            'total_time' => $total_time,
        ];
        return $data_time;
    }
    
    /**
     * @param $time_id
     * @param $end_time
     */
    static function editTime($time_id, $time, $event)
    {
        $time_model = Times::findFirst([
            'conditions' => 'id = :id:',
            'bind' => [
                'id' => $time_id,
            ]
        ]);
    
        $users_first_time_table = Times::findFirst([
            'conditions' => 'user_id = :user_id: AND date = :date:',
            'bind' => [
                'user_id' => $time_model->user_id,
                'date' => $time_model->date
            ]
        ]);
        
        if ($event == 'start-input') {
            if ($time_model->id === $users_first_time_table->id) {
                $is_late = 0;
                if (strtotime($time) >= strtotime("09:00")) {
                    $is_late = 1;
                }
            }
            
            $time_model->total_time = self::countTotalMinutesForTimeTable($time, $time_model->end_time, $time_model->date);
            $time_model->start_time = $time;
            $time_model->is_late = $is_late;
        } else {
            $time_model->total_time = self::countTotalMinutesForTimeTable($time_model->start_time, $time, $time_model->date);
            $time_model->end_time = $time;
        }
    
        $time_model->save();
    
        $total_time = self::countTotalTime($time_model->user_id, $time_model->date);
        $data_time = [
            'user_id' => $time_model->user_id,
            'date' => $time_model->date,
            'total_time' => $total_time
        ];
        return $data_time;
        
    }
    
    /**
     * @param $start_time
     * @param $end_time
     * @param $date
     * @return float|int
     * @throws Exception
     */
    static function countTotalMinutesForTimeTable($start_time, $end_time, $date)
    {
        $start_date = new DateTime($date . ' ' . $start_time);
        $end_date = new DateTime($date . ' ' . $end_time);
        
        $interval = $end_date->diff($start_date);
        $total_time_minutes = $interval->h * 60 + $interval->i;
        
        return $total_time_minutes;
    }
    
    /**
     * @param $user_id
     * @param $date
     * @return string
     */
    static function countTotalTime($user_id, $date)
    {
        $user_times = Times::find([
            'conditions' => 'user_id = :user_id: AND date = :date:',
            'bind' => [
                'user_id' => $user_id,
                'date' => $date
            ]
        ]);
    
        $minutes = 0;
        foreach ($user_times as $time) {
            $minutes = $time->total_time + $minutes;
        }
    
        $hours = $minutes / 60;
        $total_hours = 0;
        for ($i = 1; $i <= $hours; $i++) {
            if ($minutes >= 60) {
                $total_hours = $total_hours + 1;
                $minutes = $minutes - 60;
            }
        }
        
        if (strlen($minutes) == 1) {
            $minutes = '0' . $minutes;
        }
        
        $total_time = "{$total_hours}:{$minutes}";
        
        if ($total_time == '0:00') {
            $total_time = '';
        }
        
        return $total_time;
    }
    
    /**
     * @param $start_time
     * @param $date
     * @param $user_id
     * @return bool
     * @throws Exception
     */
    static function checksForLateness($start_time, $date, $user_id)
    {
        $user_times_count = Times::find([
            'conditions' => 'user_id = :user_id: AND date = :date:',
            'bind' => [
                'user_id' => $user_id,
                'date' => $date
            ]
        ])->count();
        
        $start = new DateTime($date . ' ' . $start_time);
        $compare_date = new DateTime($date . ' ' . '9:00');
        if (strtotime($start_time) >= strtotime("9:00") && $user_times_count == 0) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * @param $user_id
     * @return int
     * @throws Exception
     */
    static function countNumberOfLateInAuthUser($user_id, $year, $month)
    {
        $time_models = Times::find([
            'conditions' => 'user_id = :user_id: AND is_late = :is_late:',
            'bind' => [
                'user_id' => $user_id,
                'is_late' => true
            ]
        ]);
        
        $count_late = 0;
        foreach ($time_models as $time_model) {
            $date = new DateTime("{$time_model->date} {$time_model->time}");
            $year_month = $date->format('Y-m');
            $current_date = new DateTime($year . '-' . $month);
            $current_year_month = $current_date->setTimezone(new DateTimeZone('Asia/Bishkek'))->format('Y-m');
            if ($year_month === $current_year_month) {
                $count_late++;
            }
        }
        
        return $count_late;
    }
    
    /**
     * @param $user_id
     * @return float|int
     * @throws Exception
     */
    static function countWorkingHoursInAuthUser($user_id, $year, $month) // Принять date из фильтра  формате 'Y-m'
    {
        $time_models = Times::find([
            'conditions' => 'user_id = :user_id:',
            'bind' => [
                'user_id' => $user_id,
            ]
        ]);
    
        $hours = 0;
        foreach ($time_models as $time_model) {
            $date = new DateTime("{$time_model->date} {$time_model->time}");
            $year_month = $date->format('Y-m');
            $current_date = new DateTime($year . '-' . $month); // Принять дату из фильтра
            $current_year_month = $current_date->setTimezone(new DateTimeZone('Asia/Bishkek'))->format('Y-m');
            if ($year_month === $current_year_month) {
                $hours = ($time_model->total_time / 60) + $hours;
            }
        }
    
        return round($hours, 2);
    }
    
    /**
     * @param $days_in_month
     * @param $year
     * @param $month
     * @return array
     * @throws Exception
     */
    static function countDaysOfMonthAndWorkingDays($days_in_month, $year, $month)
    {
        $calendar_days = Calendar::find();
        $days_of_month = [];
        $working_days = 0;
        
        for ($i = 1; $i <= $days_in_month; $i++) {
            $time = new DateTime();
            $time->setDate($year, $month, $i);
            $days_of_month[] = $time;
            $day_of_week = $time->format('D');
                if ($day_of_week != "Sat" && $day_of_week != "Sun") {
                    $working_days ++;
                }
            foreach ($calendar_days as $calendar_day) {
                $calendar_date_time = new DateTime($year . $month . $i);
                if ($calendar_day) {
                    if ($calendar_day->date === $calendar_date_time->format('Y-m-d')) {
                        $working_days --;
                    }
                }
            }
        }
        
        return $data = [
            'days_of_month' => $days_of_month,
            'working_days' => $working_days
        ];
    }
    
    static function countHoursLog($id, $current_year, $current_month, $working_hours_per_month)
    {
        $auth_user = Users::findFirst([
            'conditions' => 'id = :id:',
            'bind' => [
                'id' => $id
            ]
        ]);
        $lateness = Times::countNumberOfLateInAuthUser($auth_user->id, $current_year, $current_month);
        $total_hours = Times::countWorkingHoursInAuthUser($auth_user->id, $current_year, $current_month);
        $percentage_of_working_hours = round(100 * $total_hours / $working_hours_per_month, 2);
        
        return $data = [
            'percentage_of_working_hours' => $percentage_of_working_hours,
            'lateness' => $lateness,
            'total_hours' => $total_hours
        ];
    }
    
    /**
     * @return array
     * @throws Exception
     */
    static function getLastTenYears()
    {
        $current_date = new DateTime();
        $current_month = $current_date->format('Y');
        $years = [];
        
        for ($i = 1; $i <= 10; $i++) {
            $years[] = $current_month;
            $current_month--;
        }
        return $years;
    }
}