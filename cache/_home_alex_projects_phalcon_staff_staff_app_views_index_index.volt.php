<?= $this->flash->output() ?>
<div class="page-header mb-5 mt-5">
    <h1 class="text-center">Welcome to staff!</h1>
</div>

<?php if (($this->session->has('auth') == false)) { ?>
    <p class="text-center">Enter the application: <span class="h4"><?= $this->tag->linkTo(['auth/login', 'Login']) ?></span></p>
<?php } else { ?>
    <p class="text-center">Enter the application: <span class="h4"><?= $this->tag->linkTo(['staff/index', 'Enter']) ?></span></p>
<?php } ?>


