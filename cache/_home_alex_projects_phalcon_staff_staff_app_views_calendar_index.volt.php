<?php $i = 0; ?>
<div>
    <p class="text-center">Calendar <?= $year ?></p>
</div>
<div class="container">
    <div class="row">
        <?php foreach ($months as $month) { ?>
            <?php $i = $i + 1; ?>
        <div class="col-3 mb-5">
            <h5 class="text-center"><?= $month ?></h5>
            <table class="table-sm text-center">
                <thead>
                <tr>
                    <?php foreach ($days_of_week as $day_of_week) { ?>
                    <th scope="col"><?= $day_of_week ?></th>
                    <?php } ?>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <?php foreach ($days_of_week as $day_of_week) { ?>
                    <td>
                        <?php foreach ($all_days as $day) { ?>
                            <?php if (($day_of_week == $day->format('D')) && $month == $day->format('M')) { ?>
                            <p class="calendar" id="calendar-<?= $day->format('Y-m-d') ?>" <?php if (($this->session->get('auth')['is_admin'])) { ?> onclick="calendar('calendar', 'calendar-<?= $day->format('Y-m-d') ?>')" data-date="<?= $day->format('Y-m-d') ?>" <?php } ?>
                                <?php if (($day_of_week == 'Sat' || $day_of_week == 'Sun')) { ?> style="background-color: #FF6666" <?php } ?>
                                    <?php if (($calendar_day != null)) { ?><?php if (($calendar_day->checkDate($day->format('Y-m-d')) == $day->format('Y-m-d'))) { ?> style="background-color: greenyellow" <?php } ?> <?php } ?>>
                                <?= $day->format('d') ?>
                            </p>
                            <?php } ?>
                        <?php } ?>
                    </td>
                    <?php } ?>
                </tr>
                </tbody>
            </table>
        </div>
        <?php } ?>
    </div>
</div>

<p class="ml-5"><?= $this->tag->linkTo(['/staff/index', 'Back']) ?></p>