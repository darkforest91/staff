<div class="container mt-3">
    <h1 class="mt-3">Create a User</h1>

    <form method="post">
        <div class="form-group row">
            <label for="name" class="col-sm-2 col-form-label">Name</label>
            <div class="col-sm-10">
                <?= $form->render('name', ['class' => 'form-control', 'placeholder' => 'Name']) ?>
            </div>
        </div>

        <div class="form-group row">
            <label for="name" class="col-sm-2 col-form-label">Login</label>
            <div class="col-sm-10">
                <?= $form->render('login', ['class' => 'form-control', 'placeholder' => 'Login']) ?>
            </div>
        </div>

        <div class="form-group row">
            <label for="name" class="col-sm-2 col-form-label">Email</label>
            <div class="col-sm-10">
                <?= $form->render('email', ['class' => 'form-control', 'placeholder' => 'Email']) ?>
            </div>
        </div>

        <div class="form-group row">
            <label for="name" class="col-sm-2 col-form-label">Password</label>
            <div class="col-sm-10">
                <?= $form->render('password', ['class' => 'form-control', 'placeholder' => 'Password']) ?>
            </div>
        </div>

        <div class="form-group row">
            <label for="name" class="col-sm-2 col-form-label">Confirm password</label>
            <div class="col-sm-10">
                <?= $form->render('confirmPassword', ['class' => 'form-control', 'placeholder' => 'Confirm Password']) ?>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-10">
                <?= $form->render('csrf', ['value' => $this->security->getToken()]) ?>
                <?= $form->render('Create') ?>
                <div class="text-right">
                    <p><?= $this->tag->linkTo(['staff/index', 'Back']) ?></p>
                </div>
            </div>
        </div>
    </form>
</div>