
<div class="ml-3 mb-5 big-table">
    <table class="table table-bordered">
        <thead>
        <tr class="text-center">
            <th class="hide-show" onclick="hideShow('hide')" scope="col">Hide/Show</th>

            <?php foreach ($users as $user) { ?>
                <?php if (($this->session->get('auth')['is_admin'])) { ?>
                <th scope="col"><a href="/users/edit/<?= $user->id ?>"><?= $user->name ?></th>
                <?php } else { ?>
                    <th scope="col"><?= $user->name ?></th>
                <?php } ?>
            <?php } ?>
        </tr>
        </thead>
        <tbody style="background-color: #ECF8E0">
        <?php foreach ($days_of_month as $day) { ?>
            <tr>
                <th scope="row" <?php if (($day->format('Y-m-d') !== $current_date_time->format('Y-m-d'))) { ?> class="text-center table-hidden" hidden <?php } else { ?> class="text-center" <?php } ?>>
                    <p><?= $day->format('d') ?></p>
                    <p class="border" style="background-color: white"><?= $day->format('D') ?></p>
                </th>

                <?php foreach ($users as $user) { ?>
                    <?php $button_stop = false; ?>
                    <td
                    <?php if (($day->format('D') != 'Sat' && $day->format('D') != 'Sun')) { ?> style="background-color: #E0FFD0" <?php } else { ?> style="background-color: #FFFFE0" <?php } ?>
                    <?php if (($day->format('Y-m-d') !== $current_date_time->format('Y-m-d'))) { ?> class="table-hidden" hidden <?php } ?>>
                        <p><input type="checkbox"></p>

                        <?php if (($day->format('D') != 'Sat' && $day->format('D') != 'Sun')) { ?>
                            <div id="time-block-<?= $day->format('Y-m-d') ?>-<?= $user->id ?>">
                                <?php foreach ($user->times as $time) { ?>
                                    <div class="main-time-block-<?= $day->format('Y-m-d') ?>-<?= $user->id ?>-<?= $time->id ?>" style="text-align: center">
                                        <?php if (($time->date == $day->format('Y-m-d'))) { ?>
                                            <?php if (($this->session->get('auth')['is_admin'] == 1 && $time->end_time)) { ?>
                                                <div class="raw">
                                                    <input class="input-start-time" id="start-input-<?= $time->id ?>" onchange="input('start-input', 'start-input-<?= $time->id ?>')" data-input-start-time="<?= $time->id ?>" data-start-time="<?= $time->start_time ?>" type="text" style="width: 50px" value="<?= $time->start_time ?>">
                                                    -
                                                    <input class="input-end-time" id="stop-input-<?= $time->id ?>" onchange="input('stop-input', 'stop-input-<?= $time->id ?>')" data-input-stop-time="<?= $time->id ?>" data-end-time="<?= $time->end_time ?>" type="text" style="width: 50px" value="<?= $time->end_time ?>">
                                                </div>
                                            <?php } else { ?>
                                                <?= $time->start_time ?> - <?= $time->end_time ?>
                                            <?php } ?>
                                            <?php if (($time->start_time && $time->end_time)) { ?>
                                            <?php } else { ?>
                                                <?php $button_stop = true; ?>
                                                <span class="stop-button-block-<?= $user->id ?>-<?= $day->format('Y-m-d') ?>">
                                                    <button type="button" class="text-white stop-button" onclick="status('stop-button','stop-button-<?= $day->format('Y-m-d') ?>-<?= $user->id ?>')" id="stop-button-<?= $day->format('Y-m-d') ?>-<?= $user->id ?>" data-time-id="<?= $time->id ?>"
                                                            data-user-id="<?= $user->id ?>" data-date="<?= $day->format('Y-m-d') ?>" value="start" style="background-color: darkslategray">Stop</button>
                                                </span>
                                            <?php } ?>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                            </div>

                            <div id="start-button-block-<?= $day->format('Y-m-d') ?>-<?= $user->id ?>"></div>

                            <?php if (($button_stop == false)) { ?>
                                <?php if (($day->format('Y-m-d') == $current_date_time->format('Y-m-d')) && $this->session->get('auth')['id'] == $user->id && $this->session->get('auth')['is_admin'] == false) { ?>
                                    <span class="start-button-block-<?= $user->id ?>-<?= $day->format('Y-m-d') ?>">
                                            <button type="button" class="text-white start-button" onclick="status('start-button','start-button-<?= $day->format('Y-m-d') ?>-<?= $user->id ?>')" id="start-button-<?= $day->format('Y-m-d') ?>-<?= $user->id ?>" data-user-id="<?= $user->id ?>"
                                                    data-date="<?= $day->format('Y-m-d') ?>"
                                                    value="start" style="background-color: darkslategray">Start</button>
                                    </span>
                                <?php } elseif (($this->session->get('auth')['is_admin']) && $day->format('Y-m-d') <= $current_date_time->format('Y-m-d')) { ?>
                                    <span class="start-button-block-<?= $user->id ?>-<?= $day->format('Y-m-d') ?>">
                                            <button type="button" class="text-white start-button" onclick="status('start-button','start-button-<?= $day->format('Y-m-d') ?>-<?= $user->id ?>')" id="start-button-<?= $day->format('Y-m-d') ?>-<?= $user->id ?>" data-user-id="<?= $user->id ?>"
                                                    data-date="<?= $day->format('Y-m-d') ?>"
                                                    value="start" style="background-color: darkslategray">Start</button>
                                    </span>
                                <?php } ?>
                            <?php } ?>

                            <?php if (($user->times->count() != 0)) { ?>
                                <p class="text-center" id="total-time-<?= $user->id ?>-<?= $day->format('Y-m-d') ?>">
                                    Total: <?= $time->countTotalTime($user->id, $day->format('Y-m-d')) ?>
                                </p>
                            <?php } else { ?>
                                <p class="text-center" id="total-time-<?= $user->id ?>-<?= $day->format('Y-m-d') ?>">Total:</p>
                            <?php } ?>
                            <div class="text-center" id="total-time-block-<?= $user->id ?>-<?= $day->format('Y-m-d') ?>"></div>
                        <?php } ?>
                    </td>
                <?php } ?>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>

<p hidden id="hours-log-total-time" data-total-hours="<?= $total_hours ?>"></p>
<p hidden id="hours-log-percentage-of-working-hours" data-percentage-of-working-hours="<?= $percentage_of_working_hours ?>"></p>
<p hidden id="hours-log-working-hours-per-month" data-working-hours-per-month="<?= $working_hours_per_month ?>"></p>
<p hidden id="hours-log-lateness" data-lateness="<?= $lateness ?>"></p>