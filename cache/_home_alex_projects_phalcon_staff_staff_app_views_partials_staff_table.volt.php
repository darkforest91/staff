

<div class="ml-5">
    <table class="table table-bordered">
        <thead>
        <tr class="text-center">
            <th scope="col">Hide/Show</th>
            <?php foreach ($users as $user) { ?>
                <th scope="col"><?= $user->name ?></th>
            <?php } ?>
        </tr>
        </thead>
        <tbody style="background-color: #ECF8E0">
        <?php foreach ($days_of_month as $day) { ?>
            <tr>
                <th scope="row" class="text-center">
                    <p><?= $day->format('d') ?></p>
                    <p class="border" style="background-color: white"><?= $day->format('D') ?></p>
                </th>

                <?php foreach ($users as $user) { ?>
                    <?php $button_stop = false; ?>
                    <td style="background-color: #E0FFD0">
                        <p><input type="checkbox"></p>

                        <?php if (($day->format('D') != 'Sat' && $day->format('D') != 'Sun')) { ?>
                            <div id="time-block-<?= $day->format('Y-m-d') ?>-<?= $user->id ?>">
                                <?php foreach ($user->times as $time) { ?>
                                    <div class="main-time-block-<?= $day->format('Y-m-d') ?>-<?= $user->id ?>-<?= $time->id ?>">
                                        <?php if (($time->date == $day->format('Y-m-d'))) { ?>
                                            <?= $time->start_time ?> - <?= $time->end_time ?>
                                            <?php if (($time->start_time && $time->end_time)) { ?>
                                            <?php } else { ?>
                                                <?php $button_stop = true; ?>
                                                <span class="stop-button-block-<?= $user->id ?>-<?= $day->format('Y-m-d') ?>">
                                                        <button type="button" class="text-white stop-button" onclick="status('stop-button','stop-button-<?= $day->format('Y-m-d') ?>-<?= $user->id ?>')" id="stop-button-<?= $day->format('Y-m-d') ?>-<?= $user->id ?>" data-time-id="<?= $time->id ?>"
                                                                data-user-id="<?= $user->id ?>" data-date="<?= $day->format('Y-m-d') ?>" value="start" style="background-color: darkslategray">Stop</button>
                                                    </span>

                                            <?php } ?>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                            </div>

                            <div id="start-button-block-<?= $day->format('Y-m-d') ?>-<?= $user->id ?>"></div>

                            <?php if (($button_stop == false)) { ?>
                                <span class="start-button-block-<?= $user->id ?>-<?= $day->format('Y-m-d') ?>">
                                        <button type="button" class="text-white start-button" onclick="status('start-button','start-button-<?= $day->format('Y-m-d') ?>-<?= $user->id ?>')" id="start-button-<?= $day->format('Y-m-d') ?>-<?= $user->id ?>" data-user-id="<?= $user->id ?>"
                                                data-date="<?= $day->format('Y-m-d') ?>"
                                                value="start" style="background-color: darkslategray">Start</button>
                                    </span>
                            <?php } ?>

                            <?php if (($user->times->count() != 0)) { ?>
                                <p class="text-center" id="total-time-<?= $user->id ?>-<?= $day->format('Y-m-d') ?>">
                                    Total: <?= $time->countTotalTime($user->id, $day->format('Y-m-d')) ?>
                                </p>
                            <?php } else { ?>
                                <p class="text-center" id="total-time-<?= $user->id ?>-<?= $day->format('Y-m-d') ?>">Total:</p>
                            <?php } ?>
                            <div class="text-center" id="total-time-block-<?= $user->id ?>-<?= $day->format('Y-m-d') ?>"></div>
                        <?php } ?>
                    </td>
                <?php } ?>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>