
    <div class="row justify-content-center container mt-3">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Login</div>

                <div class="card-body">
                    <form method="post" action="">

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">Email</label>
                            <div class="col-md-6">
                                <?= $form->render('email', ['class' => 'form-control', 'id' => 'email-input', 'aria-describedby' => 'emailHelp', 'placeholder' => 'Enter email']) ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Password</label>
                            <div class="col-md-6">
                                <?= $form->render('password', ['class' => 'form-control', 'id' => 'password-input', 'placeholder' => 'Password']) ?>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <?= $form->render('Login') ?>
                            </div>
                        </div>

                        <?= $form->render('csrf', ['value' => $this->security->getToken()]) ?>

                    </form>
                </div>
            </div>
        </div>
    </div>