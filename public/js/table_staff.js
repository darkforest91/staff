function status(event,btn){
    var user_id = $('#'+btn).attr('data-user-id');
    var date = $('#'+btn).attr('data-date');
    var current_time = new Date();
    let minutes = current_time.getMinutes().toString();
    let hours = current_time.getHours().toString();

    if (minutes.length === 1) {
        minutes = '0' + current_time.getMinutes();
    }

    if (hours.length === 1) {
        hours = '0' + current_time.getHours();
    }

    var time = hours + ":" + minutes;

    var time_id = 0;
    if (event==='stop-button') {
        time_id = $('#'+btn).attr('data-time-id');
    }

    $.ajax({
        url: '/time/create',
        method: 'POST',
        data: {
            'user_id' : user_id,
            'date' : date,
            'time' : time,
            'event' : event,
            'time_id' : time_id
        },
        success: function (response_data) {
            time_id = response_data['time_id'];
            user_id = response_data['user_id'];
            date = response_data['date'];
            let total_time = response_data['total_time'];

            if(event==="start-button"){
                $('#'+event+"-" + date + '-' + user_id).remove();
                let button = `<button type="button" id="stop-button-${date}-${user_id}" onclick="status('stop-button','stop-button-${date}-${user_id}')" class="text-white stop-time-button" data-time-id="${time_id}" data-user-id="${user_id}" data-date="${date}" value="start" style="background-color: darkslategray">Stop</button>`;
                let div = `<div id="new-time-block-${time_id}">${time} - ${button}</div>`;
                $('#time-block-' + date + '-' + user_id).append(div);
            } else{
                $('#'+event+"-" + date + '-' + user_id).remove();
                let button = `<button type="button" id="start-button-${date}-${user_id}" onclick="status('start-button','start-button-${date}-${user_id}')" class="text-white stop-time-button" data-time-id="${time_id}" data-user-id="${user_id}" data-date="${date}" value="start" style="background-color: darkslategray">Start</button>`;

                if ($('.main-time-block-' + date + '-' + user_id + '-' + time_id).is('.main-time-block-' + date + '-' + user_id + '-' + time_id)) {
                    $('.main-time-block-' + date + '-' + user_id + '-' + time_id).append(time)
                } else {
                    $('#new-time-block-' + time_id).append(time);
                }

                $('#start-button-block-' + date + '-' + user_id).append(button);
                $('#total-time-' + user_id + '-' + date).remove();
                $('#total-time-block-' + user_id + '-' + date).append(`<p id="total-time-${user_id}-${date}">Total: ${total_time}</p>`);
            }
        }
    })
}

$('.date-filter').on('change', function () {
    let month = $('#month-filter').val();
    let year = $('#year-filter').val();

    $.ajax({
        url: '/staff/index',
        method: 'POST',
        data: {
            'year' : year,
            'month' : month
        },
        success: function (response) {
            $('#staff_table').html(response);
            let total_hours = $('#hours-log-total-time').attr('data-total-hours');
            let percentage_of_working_hours = $('#hours-log-percentage-of-working-hours').attr('data-percentage-of-working-hours');
            let working_hours_per_month = $('#hours-log-working-hours-per-month').attr('data-working-hours-per-month');
            let lateness = $('#hours-log-lateness').attr('data-lateness');

            $('.my-hours-log').html(
                `<div class="mb-4 my-hours-log">
                    <p class='user_hours_log'>You have: ${total_hours}</p>
                    <p class='user_hours_log'>You have/Assigned: ${percentage_of_working_hours}%</p>
                    <p class='user_hours_log'>Assigned: ${working_hours_per_month}</p>
                    <p class='user_hours_log'>You are late: ${lateness}</p>
                </div>`);
        }
    })
});

function input(event, input){
    let time_id = 0;
    let input_value = 0;
    if (event === "start-input") {
        time_id = $('#' + input).attr('data-input-start-time');
        input_value = $('#' + input).attr('data-start-time');
    } else {
        time_id = $('#' + input).attr('data-input-stop-time');
        input_value = $('#' + input).attr('data-end-time');
    }
    let time = $('#' + input).val();
    let validate = new RegExp('[0-2][0-9]:[0-5][0-9]');
    if (validate.test(time) && time.length === 5) {
        $.ajax({
            url: '/time/edit',
            method: 'POST',
            data: {
                'time_id' : time_id,
                'time' : time,
                'event' : event
            },
            success: function (response) {
                let user_id = response['user_id'];
                let date = response['date'];
                let total_time = response['total_time'];
                $('#total-time-' + user_id + '-' + date).remove();
                $('#total-time-block-' + user_id + '-' + date).append(`<p id="total-time-${user_id}-${date}">Total: ${total_time}</p>`);
            }
        });
    } else {
        $('#' + input).val(input_value);
    }
}

let hidden_state = false;
function hideShow(event) {
    if (hidden_state) {
        $('.table-hidden').attr('hidden', true);
        hidden_state = false;
    } else {
        $('.table-hidden').attr('hidden', false);
        hidden_state = true;
    }
}