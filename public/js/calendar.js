function calendar(event, onclick) {
    let date = $('#' + onclick).attr('data-date');
    $.ajax({
        url: '/calendar/store',
        method: 'POST',
        data: {
            'date' : date
        },
        success: function (response) {
            console.log(response);
            if (response) {
                $('#' + onclick).css('background-color', 'greenyellow');
            } else {
                $('#' + onclick).css('background-color', 'lightgray');
            }

        }
    })
}