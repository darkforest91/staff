-- MySQL dump 10.13  Distrib 5.7.31, for Linux (x86_64)
--
-- Host: localhost    Database: staff
-- ------------------------------------------------------
-- Server version	5.7.31-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `times`
--

DROP TABLE IF EXISTS `times`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `times` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `start_time` varchar(45) NOT NULL,
  `end_time` varchar(45) DEFAULT NULL,
  `date` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=154 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `times`
--

LOCK TABLES `times` WRITE;
/*!40000 ALTER TABLE `times` DISABLE KEYS */;
INSERT INTO `times` VALUES (1,1,'12:10','15:30','2020-12-01'),(2,1,'12:28','18:21','2020-12-02'),(3,1,'15:40','19:00','2020-12-01'),(4,2,'12:49','16:14','2020-12-01'),(5,2,'14:48','16:20','2020-12-02'),(6,2,'14:48','14:59','2020-12-03'),(149,3,'16:02','16:02','2020-12-01'),(150,4,'16:02',NULL,'2020-12-01'),(151,5,'16:02','16:02','2020-12-01'),(152,2,'16:02','16:02','2020-12-01'),(153,2,'16:14','16:14','2020-12-02');
/*!40000 ALTER TABLE `times` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `login` varchar(128) DEFAULT NULL,
  `email` varchar(200) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(200) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Alex Lupenko','','darkforest91@mail.ru',NULL,'$2y$10$Zjd5SCtWTUhjN1VNZ0RDUu9.QswMVuRs5LmByt4WSXO4IGHXs8D7e',NULL,NULL,NULL,0),(2,'John Doe','darkforest91','test@mail.ru',NULL,'$2y$10$bEMvS0JuNm5ZdWF3UFU1QO5qn8T6/oIpZP3SaOFdLTxV9APTXKjoq',NULL,NULL,NULL,1),(3,'Richard Hendrix','rich','asdf@mail.ru',NULL,'$2y$10$eHFnalJTVy9KWnhreDZmZ.k.xDyWT5vpzIcNqzbn4fXEQ8qcyknKe',NULL,NULL,NULL,0),(4,'Shannon Stevens',NULL,'adfb@mail.ru',NULL,'$2y$10$eHFnalJTVy9KWnhreDZmZ.k.xDyWT5vpzIcNqzbn4fXEQ8qcyknKe',NULL,NULL,NULL,0),(5,'Ann Mathews',NULL,'ann@gmail.mpl',NULL,'$2y$10$eHFnalJTVy9KWnhreDZmZ.k.xDyWT5vpzIcNqzbn4fXEQ8qcyknKe',NULL,NULL,NULL,0),(6,'Betty Cook',NULL,'betty@exmpl.mpl',NULL,'$2y$10$eHFnalJTVy9KWnhreDZmZ.k.xDyWT5vpzIcNqzbn4fXEQ8qcyknKe',NULL,NULL,NULL,0),(7,'Helen Lawson',NULL,'Helen@exmpl.ex',NULL,'$2y$10$eHFnalJTVy9KWnhreDZmZ.k.xDyWT5vpzIcNqzbn4fXEQ8qcyknKe',NULL,NULL,NULL,0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-12-09 16:15:09
